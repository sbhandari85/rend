=====
3.1.0
=====

Rend 3.1 fixes an issue in the new block loading process where
an empty block would cause a traceback.
